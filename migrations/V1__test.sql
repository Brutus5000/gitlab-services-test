create table test
(
	id int auto_increment,
	value int not null,
	constraint test_pk
		primary key (id)
);
